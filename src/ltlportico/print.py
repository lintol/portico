from textwrap import wrap

DEFAULT_FULL_IDS = False
DEFAULT_ELLIPSIZE_LENGTH = 8

class Printer:
    def __init__(self, options):
        self.set_options(options)

    def set_options(self, options):
        self._options = options
        self._full_ids = self._options.get('fullIds', DEFAULT_FULL_IDS)
        self._ell_ct = self._options.get('ellipsizeLength', DEFAULT_ELLIPSIZE_LENGTH)

    def fmtid(self, idx):
        if self._full_ids:
            return idx
        else:
            return self.ell(idx)

    def _to_count(self, count):
        if count is None:
            count = self._ell_ct
        elif isinstance(count, str) and count[0] == 'x':
            count = self._ell_ct * int(count[1:])

        return count

    def wrap(self, text, count=None):
        return wrap(text, width=self._to_count(count))

    def ell(self, text, count=None):
        count = self._to_count(count)

        return text[:count - 3] + '...' if len(text) > count else text
