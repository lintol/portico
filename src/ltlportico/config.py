"""
Configuration for Lintol Portico Python client framework

GNU AGPLv3
"""

import os
import logging
import yaml
import copy

CONFIGDIR = os.path.join(os.path.expanduser("~"), '.config', 'ltlportico')
CONFIGFILE = os.path.join(CONFIGDIR, 'config.yaml')
CACHEFILE = os.path.join(CONFIGDIR, 'cache.yaml')

default_config = {
    'api': {
        'url': 'https://app.lintol.io',
        'prefix': '/api/v1.0'
    }
}

def _recursive_update(target, source):
    for key, val in source.items():
        if key in target and isinstance(target[key], dict):
            if not isinstance(source[key], dict):
                raise RuntimeError('Trying to merge mismatching config dicts')
            target[key].update(_recursive_update(target[key], val))
        else:
            if isinstance(source[key], dict) or isinstance(source[key], list):
                target[key] = copy.deepcopy(val)
            else:
                target[key] = val
    return target

class Undefined:
    pass

class CacheManager:
    _cache = {}

    def forget(self, typ):
        self._cache[typ] = {}

    def add(self, typ, key, value=None, idx=None):
        if typ not in self._cache:
            self._cache[typ] = {}

        if value is None:
            value = {'key': key}
        if idx is not None:
            value['key'] = idx

        self._cache[typ][key] = value

    def find(self, typ, key_frag):
        if typ not in self._cache:
            return Undefined

        found = [k for k in self._cache[typ] if k.startswith(key_frag)]

        if len(found) == 1:
            return found[0], self._cache[typ][found[0]]

        if len(found) == 0:
            return Undefined

        raise RuntimeError(_("Too many {typ} keys (IDs?) match the fragment supplied").format(typ=typ))

    def load(self):
        os.makedirs(CONFIGDIR, exist_ok=True)
        try:
            with open(CACHEFILE, 'r') as cache_file:
                self._cache = yaml.load(cache_file, Loader=yaml.SafeLoader)
        except FileNotFoundError:
            logging.info(_("No cache file found, creating..."))
            with open(CACHEFILE, 'w') as cache_file:
                yaml.dump(self._cache, cache_file)

    def save(self):
        with open(CACHEFILE, 'w') as cache_file:
            yaml.dump(self._cache, cache_file)


class ConfigManager:
    _loaded = False
    _config = {}
    _perm_config = {}
    _config = {}
    _view = None
    _parent = None

    def load(self, include_temp=True):
        global default_config

        if self._parent:
            config = self._parent.load(include_temp=include_temp)
            self._perm_config = self._parent._perm_config
            self._config = self._parent._config
            return config

        config = _recursive_update({}, default_config)

        os.makedirs(CONFIGDIR, exist_ok=True)
        try:
            with open(CONFIGFILE, 'r') as config_file:
                loaded_config = yaml.load(config_file, Loader=yaml.SafeLoader)
            if loaded_config:
                config = _recursive_update(config, loaded_config)
        except FileNotFoundError:
            logging.info(_("No config file found, creating..."))
            with open(CONFIGFILE, 'w') as config_file:
                yaml.dump(config, config_file)

        config = _recursive_update(config, self._perm_config)
        self._perm_config = copy.deepcopy(config)

        if include_temp:
            config = _recursive_update(config, self._config)
            self._config = config

        return config

    def save(self):
        config = self.load(include_temp=False)

        with open(CONFIGFILE, 'w') as config_file:
            yaml.dump(config, config_file)

    def view(self, key, default=Undefined):
        if key not in self._config:
            if default is Undefined:
                raise RuntimeError(_('Could not find config key: ') + key)
            else:
                self.set(key, default)

        config = ConfigManager()
        config._config = self._config
        config._view = key
        return config

    def get(self, key, default=Undefined):
        if self._loaded:
            config = self._config
        else:
            config = self.load()

        if self._view:
            config = config[self._view]

        def get_next(cmpts, level):
            if cmpts[0] not in level:
                if default is Undefined:
                    raise RuntimeError(_('Could not find config key: ') + key)
                else:
                    return default
            if len(cmpts) > 1:
                return get_next(cmpts[1:], level[cmpts[0]])
            return level[cmpts[0]]

        cmpts = key.split('.')
        return get_next(cmpts, config)

    def set(self, key, value, default=None, permanent=True):
        if not self._loaded:
            self.load()
        config = self._config

        if self._view:
            config = config[self._view]

        if value is None:
            value = default
        if value is None:
            raise RuntimeError(f'No value, nor default for config setting {key}')

        cmpts = key.split('.')
        def set_in_config(config):
            level = config
            for cmpt in cmpts[:-1]:
                if cmpt not in level:
                    level[cmpt] = {}
                level = level[cmpt]
            level[cmpts[-1]] = value

        if permanent:
            set_in_config(self._perm_config)
        set_in_config(self._config)

config = ConfigManager()
cache = CacheManager()
