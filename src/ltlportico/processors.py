"""
Controller for Lintol Portico processors
"""

from collections import namedtuple
from typing import Union
from jsonapi_client import Inclusion
from tabulate import tabulate
from enum import Enum
from colorama import Fore, Style
import json

from .api import ValidationError
from .config import cache, Undefined

class Processor:
    class Status:
        INACTIVE = 0
        UNPROCESSED = 1
        IN_PROGRESS = 2
        COMPLETE = 3
        FAILED = 4

        def __init__(self, available=None, total=None):
            self._available = available
            self._total = total

        @property
        def colored(self):

            if self._available is None:
                text = '(unknown)'
                color = Fore.MAGENTA
            else:
                text = f'{self._available}/{self._total}'
                if self._available > 0:
                    if self._available == self._total:
                        color = Fore.GREEN
                    else:
                        color = Fore.RED
                else:
                    color = Fore.YELLOW

            return color + text + Style.RESET_ALL

    def __init__(self, idx, unique_tag, name, status,
            configuration_count, description=None, definition=None,
            supplementary=None, configuration_options=None,
            configuration_defaults=None):
        self.id = idx
        self.unique_tag = unique_tag
        self.name = name
        self.definition = definition
        self.configuration_options = configuration_options
        self.configuration_defaults = configuration_defaults

        self.supplementary = supplementary

        if type(status) is bool and not status:
            self.status = self.Status()
        else:
            self.status = self.Status(status.available, status.total)

        if description is None:
            description = ''

        self.configuration_count = configuration_count
        self.description = description

    def who_am_i(self, printer):
        return f'{self.unique_tag} [{printer.fmtid(self.id)}]'

    @classmethod
    def from_resource(cls, res):
        cache.add('processors', res.id)
        cache.add('processors', res.uniqueTag, idx=res.id)

        if hasattr(res, 'definition'):
            definition = json.loads(res.definition)
        else:
            definition = None

        if hasattr(res, 'configurationDefaults'):
            configuration_defaults = res.configurationDefaults
        else:
            configuration_defaults = None

        if hasattr(res, 'configurationOptions'):
            configuration_options = res.configurationOptions

            # LEGACY FORMAT
            if type(configuration_options) is str:
                configuration_options = json.loads(configuration_options)
                if configuration_options:
                    configuration_options = list(zip(*list(configuration_options.items())))
                    configuration_options = namedtuple('ConfigurationOptions', configuration_options[0])(*configuration_options[1])
                else:
                    configuration_options = namedtuple('ConfigurationOptions', 'fields')([])
        else:
            configuration_options = None

        if hasattr(res, 'supplementaryLinks'):
            supplementary = json.loads(res.supplementaryLinks)
        else:
            supplementary = None

        return cls(
            idx=res.id,
            unique_tag=res.uniqueTag,
            name=res.name,
            description=res.description,
            status=res.status,
            configuration_count=res.configurationCount,
            definition=definition,
            supplementary=supplementary,
            configuration_options=configuration_options,
            configuration_defaults=configuration_defaults
        )

    @classmethod
    def from_resources(cls, ress):
        return [cls.from_resource(res) for res in ress]

class ProcessorController:
    def __init__(self, api):
        self._api = api

    def create(self, unique_tag, name, description, definition=None, function=None, supplementary=None, configuration_options=None,
            configuration_defaults=None):
        if definition is None:
            definition = {}

        if configuration_options is None:
            configuration_options = {}

        if configuration_defaults is None:
            configuration_defaults = {}

        if supplementary is None:
            supplementary = {}
        else:
            if 'dependencies' not in definition:
                definition['dependencies'] = []
            definition['dependencies'] += [f'$->{key}' for key in supplementary]

        if function is not None:
            if 'docker' not in definition:
                definition['docker'] = {}
            definition['docker']['image'] = function
            definition['docker']['revision'] = 'latest'

        definition = json.dumps(definition)
        configuration_options = json.dumps(configuration_options)
        configuration_defaults = json.dumps(configuration_defaults)

        processor = self._api.create(
            'processors',
            uniqueTag=unique_tag,
            name=name,
            description=description,
            definition=definition,
            supplementaryLinks=supplementary,
            configurationOptions=configuration_options,
            configurationDefaults=configuration_defaults
        )
        return Processor.from_resource(processor.resource)

    def destroy(self, identifier):
        processor = self._api.destroy('processors', identifier)
        return Processor.from_resource(processor)

    def get(self, idx=None, only_mine=False):
        if idx is None:
            cache.forget('processors')

            flags = set()
            if only_mine:
                flags.add('onlyMine')

            processors = self._api.index('processors', flags=flags)

            return Processor.from_resources(processors.resources)
        else:
            processor = self._api.show('processors', idx, fields=['definition', 'supplementaryLinks'])

            return Processor.from_resource(processor.resource)


class ProcessorView:
    def __init__(self, printer):
        self._printer = printer

    def show(self, processor: Processor, action=None) -> str:
        p = self._printer
        rows = [
            ('PROCESSOR', processor.who_am_i(p)),
            (_('Name'), processor.name),
            (_('Status'), processor.status.colored),
            (_('Profiles'), processor.configuration_count),
            (_('Description'), '\n'.join(p.wrap(processor.description, 'x10')))
        ]

        if processor.definition:
            rows.append((_('Definition'), processor.definition))

        if processor.supplementary:
            rows.append((_('Supplementary Links'), len(processor.supplementary)))
            for code, link in processor.supplementary.items():
                rows.append((f'|__{code}', link))

        if processor.configuration_options:
            fields = ', '.join([f['label'] for f in processor.configuration_options.fields])
            rows.append((f'Configuration Fields', fields))

        if processor.configuration_defaults:
            metadata_only = 'metadataOnly' in processor.configuration_defaults and processor.configuration_defaults['metadataOnly']
            fields = '\n'.join([f'{k} : {v}' for k, v in processor.configuration_defaults.items() if k != 'metadataOnly'])
            rows.append((f'Configuration Defaults', fields))
        else:
            metadata_only = False

        rows.append(('Metadata Only', 'Y' if metadata_only else 'N'))

        table = tabulate(rows)

        if action:
            table = f'{Fore.YELLOW}{_(action.upper())}{Style.RESET_ALL}\n\n' + table

        return table

    def table(self, processors) -> str:
        p = self._printer
        headers = [
            _(''),
            _('Name'),
            _('Status'),
            _('Profiles'),
            _('Fields'),
            _('Description')
        ]
        rows = []

        for processor in processors:
            if processor.configuration_options:
                field_count = len(processor.configuration_options.fields)
            else:
                field_count = ''

            rows.append([
                processor.who_am_i(p),
                processor.name,
                processor.status.colored,
                processor.configuration_count,
                field_count,
                p.ell(processor.description, count='x4')
            ])
        table = tabulate(rows, headers=headers)

        return table

    def errors(self, error: ValidationError) -> str:
        text = f'{Fore.RED}{_("ERRORS")}{Style.RESET_ALL}\n{error.message}\n\n'

        rows = []
        for key, errors in error.errors.items():
            rows.append([
                f'{Fore.YELLOW}{key}{Style.RESET_ALL}',
                '\n'.join(errors)
            ])
        table = text + tabulate(rows)

        return table
