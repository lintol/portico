"""
Configuration for Lintol Portico Python client framework

GNU AGPLv3
"""

import click
import gettext
import logging
import json
import getpass
import colorama
import os
import subprocess
from functools import update_wrapper
from ltlportico import commands
from ltlportico.config import config as init_config, cache
from ltlportico.api import Api
from ltlportico.print import Printer
from ltlportico.processors import ProcessorController, ProcessorView

LOGGING_FORMAT = '%(asctime)-15s %(message)s'

def context(f):
    @click.pass_context
    def new_func(ctx, *args, **kwargs):
        result = ctx.invoke(f, ctx, *args, **kwargs)
        ctx.obj['config'].save()
        cache.save()
        return result
    return update_wrapper(new_func, f)

@click.group()
@click.option('--debug/--no-debug', default=False)
@click.option('--full-ids/--no-full-ids', '-i', default=None)
@context
def cli(ctx, debug, full_ids):
    gettext.install('ltldoorstep')
    colorama.init()

    init_config.load()
    cache.load()

    if full_ids is not None:
        init_config.set('print.fullIds', full_ids, permanent=False)

    view_options = {
        'full_ids'
    }

    logging.basicConfig(level=logging.DEBUG if debug else logging.INFO, format=LOGGING_FORMAT)
    logger = logging.getLogger(__name__)

    json_logger = logging.getLogger('jsonapi_client')
    json_logger.setLevel(logging.ERROR)

    printer = Printer(init_config.view('print', {}))

    ctx.obj = {
        'DEBUG': debug,
        'config': init_config,
        'logger': logger,
        'printer': printer,
        'api': Api(init_config)
    }

def welcome_message():
    msg = 'Lintol Portico: ' + _('CLI Tool')
    return msg + '\n' + '=' * len(msg)

@cli.command()
@context
def login(ctx):
    click.echo(welcome_message())

    config = ctx.obj['config']

    server = config.get('api.url')
    click.echo(f'Logging into {server}')

    api_token = input('API Token: ')

    config.set('api.token', api_token)

    config.save()

@cli.group()
@context
def processors(ctx):
    ctx.obj['controller'] = ProcessorController(ctx.obj['api'])
    ctx.obj['view'] = ProcessorView(ctx.obj['printer'])

def generic_get(ctx, idx=None, options={}):
    controller = ctx.obj['controller']
    view = ctx.obj['view']

    commands.get(controller, view, idx, options)

def generic_create(ctx, **attributes):
    controller = ctx.obj['controller']
    view = ctx.obj['view']

    commands.create(controller, view, attributes)

def generic_delete(ctx, **attributes):
    controller = ctx.obj['controller']
    view = ctx.obj['view']

    commands.delete(controller, view, attributes)

@processors.command()
@click.option('--only-mine/--no-only-mine', '-m', default=False)
@click.argument('idx', nargs=-1)
@context
def get(ctx, idx, only_mine):
    idx = idx[0] if idx else None

    generic_get(ctx, idx, {'only_mine': only_mine})

@processors.command()
@click.argument('function')
@click.argument('unique-tag')
@click.argument('openfaas-yml')
@context
def up(ctx, function, unique_tag, openfaas_yml):
    p = subprocess.Popen([
        'faas-cli',
        'up',
        '-f',
        openfaas_yml,
        '--filter',
        function
    ])
    p.wait()

@processors.command()
@click.option('--description', '-D', default=None)
@click.option('--definition', '-d', default=None)
@click.option('--configuration-options', '-c', default=None)
@click.option('--configuration-defaults', '-C', default=None)
@click.option('--metadata-only/--no-metadata-only', '-M', default=None)
@click.option('--function', '-f', default=None)
@click.option('--supplementary', '-s', multiple=True, help="Format: CODE>URL")
@click.argument('unique-tag')
@click.argument('name')
@context
def create(ctx, unique_tag, name, description, definition, configuration_options, configuration_defaults,
            function, supplementary, metadata_only):
    if definition:
        definition = json.loads(definition)

    if configuration_options:
        if os.path.exists(configuration_options):
            with open(configuration_options, 'r') as f:
                configuration_options = json.load(f)
        else:
            configuration_options = json.loads(configuration_options)

    if configuration_defaults:
        if os.path.exists(configuration_defaults):
            with open(configuration_defaults, 'r') as f:
                configuration_defaults = json.load(f)
        else:
            configuration_defaults = json.loads(configuration_defaults)
    elif metadata_only:
        configuration_defaults = {}

    if metadata_only:
        configuration_defaults['metadataOnly'] = True

    if len(supplementary):
        supplementary = [pair.split('>') for pair in supplementary]
        supplementary = {pair[0]: pair[1] for pair in supplementary}

    generic_create(
        ctx,
        unique_tag=unique_tag,
        name=name,
        description=description,
        definition=definition,
        function=function,
        supplementary=supplementary,
        configuration_options=configuration_options,
        configuration_defaults=configuration_defaults
    )

@processors.command()
@click.argument('identifier')
@context
def delete(ctx, identifier):
    generic_delete(ctx, identifier=identifier)
