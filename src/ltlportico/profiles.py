"""
Controller for Lintol Portico profiles

GNU AGPLv3
"""

from tabulate import tabulate


class Profile:
    def __init__(self, idx, name):
        self.id = idx
        self.name = name

    def who_am_i(self, printer):
        return f'{self.name} [{printer.fmtid(self.id)}]'

    @classmethod
    def from_resource(cls, res):
        return cls(res.id, res.name)

    @classmethod
    def from_resources(cls, ress):
        return [cls.from_resource(res) for res in ress]

class ProfileController:
    def __init__(self, api):
        self._api = api

    def get(self):
        profiles = self._api.index('profiles')
        return Profile.from_resources(profiles.resources)


class ProfileView:
    def __init__(self):
        pass

    def table(self, profiles):
        headers = [
            _('ID'),
            _('Name')
        ]
        rows = []

        for profile in profiles:
            rows.append([
                profile.id,
                profile.name
            ])
        table = tabulate(rows, headers=headers)

        return table
