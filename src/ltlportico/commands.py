from .api import ValidationError

def get(controller, view, idx=None, options={}):
    if idx is not None:
        entity = controller.get(idx)
        print(view.show(entity))
    else:
        entities = controller.get(**options)
        print(view.table(entities))

def create(controller, view, attributes):
    try:
        entity = controller.create(**attributes)
        print(view.show(entity, action='created'))
    except ValidationError as e:
        print(view.errors(e))

def delete(controller, view, attributes):
    entity = controller.destroy(**attributes)
    print(view.show(entity, action='deleted'))
