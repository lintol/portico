"""
API Controller for Lintol Portico

GNU AGPLv3
"""

import logging
import requests
from jsonapi_client import Session, Inclusion, Modifier
from jsonapi_client.exceptions import DocumentError
from .config import cache, Undefined

models_as_jsonschema = {
    'processors': {'properties': {
        'name': {'type': 'string'},
        'description': {'type': 'string'},
        'definition': {'type': 'string'},
        'supplementaryLinks': {'type': 'string'},
        'configurationOptions': {'type': 'string'},
        'configurationDefaults': {'type': 'string'}
    }}
}

class ValidationError(Exception):
    def __init__(self, validation_errors, *args, **kwargs):
        self.message = validation_errors['message']
        self.errors = validation_errors['errors']
        super(ValidationError).__init__(*args, **kwargs)

class InclusionWithId(Inclusion):
    def __init__(self, idx, *include_args: 'IncludeKeywords') -> None:
        self._idx = idx
        Inclusion.__init__(self, *include_args)

    def url_with_modifiers(self, base_url: str) -> str:
        base_url = f'{base_url}/{self._idx}'
        return Inclusion.url_with_modifiers(self, base_url)

class ModifierWithId(Modifier):
    def __init__(self, idx, *modifier_args: 'ModifierKeywords') -> None:
        self._idx = idx
        Modifier.__init__(self, *modifier_args)

    def url_with_modifiers(self, base_url: str) -> str:
        base_url = f'{base_url}/{self._idx}'
        return Modifier.url_with_modifiers(self, base_url)

class Api:
    def __init__(self, config):
        self._config = config

        self.start_session()

    def start_session(self):
        root = self._config.get('api.url')
        prefix = self._config.get('api.prefix')
        token = self._config.get('api.token')
        self._session = Session(
            f'{root}{prefix}',
            request_kwargs={
                'headers': {
                    'Authorization': f'Bearer {token}',
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                }
            }
        )

    def index(self, resource, include=None, flags=None):
        if include:
            inclusion = Inclusion(*include)
        else:
            inclusion = None

        if flags:
            modifier = Modifier('&'.join([f'filter[{flag}]=1' for flag in flags]))

            if inclusion:
                inclusion += modifier
            else:
                inclusion = modifier

        return self._session.get(resource, inclusion)

    def show(self, resource, idx, include=None, fields=[]):
        cidx = cache.find(resource, idx)
        if cidx is not Undefined:
            idx = cidx[1]['key']

        inclusion = idx
        if include:
            inclusion = InclusionWithId(idx, *include)

        if fields:
            modifier = ModifierWithId(idx, '&'.join([f'fields[{resource}]={v}' for v in fields]))

            if include:
                inclusion += modifier
            else:
                inclusion = modifier

        return self._session.get(resource, inclusion)

    def destroy(self, resource, idx):
        cidx = cache.find(resource, idx)
        if cidx is not Undefined:
            idx = cidx[1]['key']

        resource = self._session.get(resource, idx).resource
        resource.delete()
        resource.commit()
        return resource

    def create(self, resource, include=None, **attributes):
        entity = self._session.create(resource)

        for key, value in attributes.items():
            setattr(entity, key, value)

        try:
            entity.commit()
        except DocumentError as e:
            if e.errors['status_code'] == 422:
                validation_errors = e.response.json()
                raise ValidationError(validation_errors)
            else:
                raise e

        idx = entity.id
        if include:
            inclusion = InclusionWithId(idx, *include)
        else:
            inclusion = idx

        entity = self._session.get(resource, inclusion)

        return entity
