FROM python:3.6

COPY requirements.txt /

RUN pip3 install -r /requirements.txt

COPY . /lintol_client_py

RUN adduser lintol

WORKDIR /lintol_client_py

RUN python3 setup.py develop

USER lintol

ENTRYPOINT [ "/lintol/run.sh" ]
